# Yotel Docker configurations

## HAProxy

To make this work you need to tie all of the related containers into a docker network in docker-compose

```
docker network create [docker network]
```

then bring all the containers up

For wordpress installs settings should be:
SITEURL=https://domainname.tld
HOME=https://domainname.tld

Change config/haproxy.cfg as follows:

Add a new backend section
 ```
backend [backend name]
  mode http
  balance leastconn
  option httpclose
  # it's using port 80 because it should be on the same docker network
  server s1 [docker container]:80 check port 80
 ```

Add snippet to frontend section
 ```
  acl is_[domainname] hdr_beg(host) -i domainname.tld
  use_backend [backend name] if is_[domainname]
 ```

### set up certificates
```
docker exec haproxy-certbot certbot-certonly \
  --domain ascentscottsdale.com \
  --domain www.ascentscottsdale.com \
  --domain staging.ascentscottsdale.com \
  --email accounts@storydriven.com \
  --dry-run --expand
```

```
docker exec haproxy-certbot haproxy-refresh
```
